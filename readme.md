# Database

The database data can be altered in the DB class: classes/databaseconnect.php


# Libraries

Run in your terminal:

```
composer install
```


# Mollie payments

Mollie needs a valid public url to update the payment status. If you don't use ngrok or something similar, your payment will always fail.
You can chance your mollie api key in the boot.php on line 12.