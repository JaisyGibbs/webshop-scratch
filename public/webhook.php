<?php

try {

	require "../boot.php";

	// id van mollie ophalen
    $payment = $mollie->payments->get($_POST["id"]);
    $order_id = $payment->metadata->order_id;

    // update de order status
    update("UPDATE orders SET payment_status = :payment_status, updated_at = :updated_at WHERE id = :id",
            [
            	"payment_status" => $payment->status,
                "updated_at" => date('Y-m-d H:i:s'),
                "id" => $order_id
            ]);
}
catch (\Mollie\Api\Exceptions\ApiException $e) {
    echo "API call failed: " . htmlspecialchars($e->getMessage());
};