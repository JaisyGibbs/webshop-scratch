<?php
	require '../boot.php';
	//3 producten uit de database halen om te weergeven
	$stmt =  select("SELECT * FROM products ORDER BY ID DESC LIMIT 3");

?>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Flip FLop Shop</title>

		<?php echo getcss();?>

	</head>

	<body>
	<?php include "partials/navbar.php"?>

		<div class="container">
			<div class="row my-4">
				<div class="col-lg-8">
					<img class="img-fluid rounded" src="images/Cariris-Wedge1111.png" alt="">
				</div>
				<div class="col-lg-4">
					<aside id="bucket">
						<?php include 'partials/bucket.php'?>
					</aside>
				</div>
			</div>

			<div class="card text-white bg-secondary my-4 text-center">
				<div class="card-body">
					<h2>The official Flip FLop Shop</h2>
				</div>
			</div>
			<div class="row">
			<?php foreach($stmt as $product){ ?>
				<div class="col-md-4 mb-4">
					<div class="card h-100">
						<div class="card-body">
								<a href="product/<?php echo $product['slug']?>">
									<img class="homeproduct" src="<?php echo $product['image']?>">
								</a>
							<h4 class="card-title"><?php echo $product['title']?></h4>
						</div>
						<div class="card-footer">
							<h4>€<?php echo $product['price']?></h4>
							<button type="button" class="btn btn-warning- add-to-cart" data-url="cart/add.php?id=<?php echo $product['id']?>">In winkelwagen</button>
						</div>
					</div>
				</div>
			<?php } ?>
			</div>
		</div>

		<footer class="py-5 bg-dark">
			<div class="container">
				<p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
			</div>
		</footer>

	</body>
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script>
	$(document).ready(function() {

		bucket();
	});


	function bucket()
	{
		$('.add-to-cart, .remove-from-cart, .reset-cart').unbind('click').click(function(event) {
			event.preventDefault();

			// alert($(this).data('url'));

			jQuery.ajax($(this).data('url'), {
				method: 'post',
				cache: false,
				// dataType: 'json',
			})
			.done(function(data) {
				if(data) {
					$('#bucket').html(data);
					bucket();
				}
			})
			.fail(function() {
				alert( "error" );
				bucket();
			});
		});
	}
	</script>
</html>
