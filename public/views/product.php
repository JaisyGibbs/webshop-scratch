<?php

	require"../../boot.php";
	//product selecteren met slug
	$product = singleselect('SELECT * FROM products where slug = :slug', ['slug'=>$_GET['slug']]);

?>
<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Flip FLop Shop</title>

		<?php echo getcss();?>

	</head>
	<body>
	<?php require"../partials/navbar.php"?>;

		<div class="container">
			<div class="card text-white bg-secondary my-4 text-center">
				<div class="card-body">
					<h2>The official Flip FLop Shop</h2>
				</div>
			</div>
			<?php if($product){ ?>
			<div class="row my-4">
				<div class="col-lg-8">
					 <div class="card-body">
          			      <img class="productimg" src="<?php echo asset($product['image'])?>">
        		    </div>
    			    <div class="card-footer card-height">
              			<h4 class="card-title float-left"><?php echo $product['title']?></h4>
              			<div class="float-right">
                			<h4>€<?php echo $product['price']?></h4>
                			<button type="button" class="btn btn-warning- add-to-cart" data-url="../cart/add.php?id=<?php echo $product['id']; ?>">Add to cart</button>
              			</div>
        			</div>
				</div>
				<?php } else { ?>
					<p>dit product bestaat niet</p>

				<?php } ?>

				<div class="col-lg-4">
					<aside id="bucket">
						<?php include '../partials/bucket.php'?>
					</aside>
	            </div>
	        </div>
        </div>
		<footer class="py-5 bg-dark">
			<div class="container">
				<p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
			</div>
		</footer>

	</body>
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script>
		$(document).ready(function() {

			bucket();
	});

	function bucket()
	{
		$('.add-to-cart, .remove-from-cart, .reset-cart').unbind('click').click(function(event) {
			event.preventDefault();
			// alert($(this).data('url'));
			jQuery.ajax($(this).data('url'), {
				method: 'post',
				cache: false,
				// dataType: 'json',
			})
			.done(function(data) {
				if(data) {
					$('#bucket').html(data);
					bucket();
				}
			})
			.fail(function() {
				alert( "error" );
				bucket();
			});
		});
	}
	</script>
</html>





