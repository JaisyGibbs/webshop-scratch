<?php
	require '../../../boot.php';

	// als de afreken button in ingklikt kunnen we gaan betalen
	if($_SERVER['REQUEST_METHOD'] === 'POST') {
		if(account::checkLoginFromSession() && $_SESSION['cart']['total'] > 0 ){
			require"../../../app/payment/pay.php";
		}
		else{
			$error = "U heeft niks in uw winkelmandje";
		}
	}
?>
<!DOCTYPE html>
<html lang="en">

	<head>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Flip FLop Shop</title>

		<?php echo getcss();?>
	</head>
	<body>
	<?php require "../../partials/navbar.php" ?>

		<div class="container">
			<br/>

			<?php if(@$error){?>
				<p class="text-danger"><?php echo $error?></p>
			<?php } ?>

			<br/>
			<aside id="bucket">
				<?php require "../../partials/bucket.php";?>
			</aside>

			<?php if(! account::checkLoginFromSession()){ ?>
				<p>U bent nog niet ingelogd, Wilt u <a href="<?php echo asset('login')?>?fromPay=true">inloggen</a> of een <a href="<?php echo asset('registreren')?>?fromPay=true">nieuw account maken</a>.</p>
			<?php }else{ ?>
				<form method="POST">
					<button type="submit" class="btn btn-primary" style="margin-top:20px;">Afrekenen</button>
				</form>
			<?php } ?>

			<br/>
		</div>

		<footer class="py-5 bg-dark">
		   <div class="container">
				<p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
		  </div>
		</footer>

    </body>
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script>
	$(document).ready(function() {
	    bucket();
	    document.getElementsByClassName('betalen')[0].style.visibility = 'hidden';
	});

	function bucket()
	{
		document.getElementsByClassName('betalen')[0].style.visibility = 'hidden';

	    $('.add-to-cart, .remove-from-cart, .reset-cart').unbind('click').click(function(event) {
		    event.preventDefault();

		    // alert($(this).data('url'));

		    jQuery.ajax($(this).data('url'), {
			    method: 'post',
			    cache: false,
			    // dataType: 'json',
		    })
		    .done(function(data) {
			    if(data) {
			   		$('#bucket').html(data);
				  	bucket();
			  	}
		  	})
		  	.fail(function() {
				alert( "error" );
			 	bucket();
		  	});

	  	});
	}

	</script>

</html>

