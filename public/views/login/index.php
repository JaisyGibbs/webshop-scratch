<?php

	require"../../../boot.php";

	$errors = [];

	if($_SERVER['REQUEST_METHOD'] === 'POST') {

		$variables = [
			'email' => ['required', 'email', 'min:7', 'max:155'],
			'password' => ['required', 'min:8', 'max:100'],
		];

		require '../../../app/validation/login.php';

		if(count($errors) == 0) {
		$error = account::login($_POST['email'], $_POST['password']);

			if(@$_GET['fromPay'] && account::checkLoginFromSession()){
				header('Location: '.asset("betalen"));
				dd();
			}

		}
	}

	function value($key)
	{
		return @$_POST[$key];
	}

	checkLogin();

?>
<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Flip FLop Shop</title>
		<?php echo getcss();?>

    </head>

    <body>
	<?php require "../../partials/navbar.php" ?>

		<div class="container cforms">
			<h2>Login</h2>
			<?php echo (@$error) ? '<p class="col-sm-10 text-danger">'.$error.'</p>' : ''; ?>
			<form class="form-horizontal" method="POST">
		  		<div class="form-group">
					<label class="control-label col-sm-2" for="email">Email:</label>
					<div class="col-sm-10">
			  			<input type="email" name="email" class="form-control" value="<?php echo value('email')?>"placeholder="Enter email">
					</div>
		  		</div>
		  		<?php echo (@$errors['email']) ? '<p class="col-sm-10 text-danger">'.$errors['email'][0].'</p>' : ''; ?>

		  		<div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Password:</label>
					<div class="col-sm-10">
			  			<input type="password" name="password" class="form-control" placeholder="Enter password">
					</div>
		  		</div>
		  		<?php echo (@$errors['password']) ? '<p class="col-sm-10 text-danger">'.$errors['password'][0].'</p>' : ''; ?>

		  		<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
			  			<button type="submit" class="btn btn-default">Inloggen</button>
					</div>
		  		</div>
			</form>
		</div>

		<footer class="py-5 bg-dark">
			<div class="container">
				<p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
			</div>
		</footer>


  	</body>
  	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</html>
