<?php
	require '../../../boot.php';

	$errors = [];

	//validatie
	if($_SERVER['REQUEST_METHOD'] === 'POST') {

		$variables = [
			'first_name' => ['required', 'min:2', 'max:50', 'name'],
			'suffix_name' => ['min:1', 'max:15', 'name'],
			'last_name' => ['required', 'name', 'min:2', 'max:50'],
			'country' => ['required', 'min:2', 'max:15', 'name'],
			'city' => ['required', 'min:2', 'max:55', 'name'],
			'street' => ['required', 'min:2', 'max:85', 'name'],
			'street_number' => ['required', 'min:1', 'max:5'],
			'street_suffix' => ['min:1', 'max:25'],
			'zipcode' => ['required', 'postcode', 'min:6', 'max:7'],
			'email' => ['required', 'email', 'min:7', 'max:155'],
			'password' => ['required', 'min:8', 'max:100', 'confirmed'],
		];

		require '../../../app/validation/validations.php';

		if(count($errors) == 0) {
			//password hashen
			$hashedPassword = password_hash($_POST['password'], PASSWORD_BCRYPT);

			//zipcode correct maken
			$correctZipcode = trim(standardizePostcode($_POST['zipcode']));

			//user aanmaken
			Account::createUser(
							trim(strtolower($_POST['email'])),
							$hashedPassword,
							'',
							standardizeName($_POST['first_name']),
							trim($_POST['suffix_name']),
							standardizeName($_POST['last_name']),
							$_POST['country'],
							standardizeName($_POST['city']),
							standardizeName($_POST['street']),
							$_POST['street_number'],
							trim($_POST['street_suffix']),
							$correctZipcode,
							date("Y-m-d H:i:s"),
							date("Y-m-d H:i:s")
						  );
			//als het van betalen scherm komt terug sturen
			if(@$_GET['fromPay']){
			  header('Location: '.asset("betalen"));
			  dd();
		   }


		}

	}
	checkLogin();
	function value($key)
	{
		return @$_POST[$key];
	}

?>
<!DOCTYPE html>
<html lang="en">

 	<head>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Flip FLop Shop</title>

		<?php echo getcss();?>

    </head>
    <body>
	<?php require "../../partials/navbar.php" ?>

		<div class="container cforms">
		    <h2>Registreren</h2>
	  		<?php if(count($errors) != 0) { ?>
			<div class="alert alert-danger">
		  		Er is iets niet correct ingevuld.
			</div>
	  		<?php } ?>
			<form class="" method="POST">
			    <div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Voornaam:</label>
					<div class="col-sm-10">
					    <input type="text" class="form-control" name="first_name"placeholder="Voornaam" value="<?php echo value('first_name')?>">
					</div>
			    </div>
			    <?php echo (@$errors['first_name']) ? '<p class="col-sm-10 text-danger">'.$errors['first_name'][0].'</p>' : ''; ?>

			    <div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Tussen voegsel:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="suffix_name" placeholder="Tussen voegsel" value="<?php echo value('suffix_name')?>">
					</div>
			  	</div>
			  	<?php echo (@$errors['suffix_name']) ? '<p class="col-sm-10 text-danger">'.$errors['suffix_name'][0].'</p>' : ''; ?>

				<div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Achternaam:</label>
					<div class="col-sm-10">
				  		<input type="text" class="form-control" name="last_name" placeholder="Achternaam" value="<?php echo value('last_name')?>">
					</div>
			  	</div>
			  	<?php echo (@$errors['last_name']) ? '<p class="col-sm-10 text-danger">'.$errors['last_name'][0].'</p>' : ''; ?>

			  	<div class="form-group">
					<label class="control-label col-sm-2" for="email">Email:</label>
					<div class="col-sm-10">
				  		<input type="tekst  " class="form-control" name="email" placeholder="email" value="<?php echo value('email')?>">
					</div>
			  	</div>
			  	<?php echo (@$errors['email']) ? '<p class="col-sm-10 text-danger">'.$errors['email'][0].'</p>' : ''; ?>

			  	<div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Wachtwoord:</label>
					<div class="col-sm-10">
				  		<input type="password" class="form-control" name="password" placeholder="Wachtwoord">
					</div>
			  	</div>
			  	<?php echo (@$errors['password']) ? '<p class="col-sm-10 text-danger">'.$errors['password'][0].'</p>' : ''; ?>

			  	<div class="form-group">
					<label class="control-label col-sm-2" for="pwd" max-width="100%">Wachtwoord Herhalen:</label>
					<div class="col-sm-10">
				  		<input type="password" class="form-control" name="password_confirmed" placeholder="Wachtwoord">
					</div>
			  	</div>

			  	<?php require "../../partials/select.php"?>

			  	<div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Stad:</label>
					<div class="col-sm-10">
					  	<input type="text" class="form-control" name="city" placeholder="Stad" value="<?php echo value('city')?>">
					</div>
			  	</div>
			  	<?php echo (@$errors['city']) ? '<p class="col-sm-10 text-danger">'.$errors['city'][0].'</p>' : ''; ?>

			  	<div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Straat:</label>
					<div class="col-sm-10">
				  		<input type="text" class="form-control" name="street" placeholder="Straat" value="<?php echo value('street')?>">
					</div>
			  	</div>
			  	<?php echo (@$errors['street']) ? '<p class="col-sm-10 text-danger">'.$errors['street'][0].'</p>' : ''; ?>

			  	<div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Straat nummer:</label>
					<div class="col-sm-10">
				  		<input type="number" class="form-control" name="street_number" placeholder="Straat Nummer" value="<?php echo value('street_number')?>">
					</div>
			  	</div>
			  	<?php echo (@$errors['street_number']) ? '<p class="col-sm-10 text-danger">'.$errors['street_number'][0].'</p>' : ''; ?>

			  	<div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Straat toevoeging:</label>
					<div class="col-sm-10">
				  		<input type="text" class="form-control" name="street_suffix" placeholder="Straat voegsel" value="<?php echo value('street_suffix')?>">
					</div>
			  	</div>
			  	<?php echo (@$errors['street_suffix']) ? '<p class="col-sm-10 text-danger">'.$errors['zipcode'][0].'</p>' : ''; ?>

			  	<div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Postcode:</label>
					<div class="col-sm-10">
				  		<input type="text" class="form-control" name="zipcode" placeholder="Postcode" value="<?php echo value('zipcode')?>">
					</div>
			  	</div>
			  	<?php echo (@$errors['zipcode']) ? '<p class="col-sm-10 text-danger">'.$errors['zipcode'][0].'</p>' : ''; ?>

			  	<button type="submit" class="btn btn-default">Registreren</button>
			</form>

		</div>

		<footer class="py-5 bg-dark">
   			<div class="container">
				<p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
		  	</div>
		</footer>

    </body>
    <script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</html>
