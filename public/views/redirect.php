<?php

	require"../../boot.php";

	$order = singleselect('SELECT * FROM orders where id = :id', ['id'=>$_GET['orderid']]);

?>
<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Flip FLop Shop</title>

		<?php echo getcss();?>

	</head>
	<body>
	<?php require"../partials/navbar.php"?>;

		<div class="container">
			<div class="card text-white bg-secondary my-4 text-center">
				<div class="card-body">
					<h2>The official Flip FLop Shop</h2>
				</div>
			</div>
			<?php if($order){ ?>
			<div class="row my-4">
				<div class="col-lg-8">
					<h3>Order <?php echo $order['id']?></h3>
					<p>Bedankt voor uw bestelling uw betalingstatus is <?php echo $order['payment_status']?></p>
				</div>
			</div>
			<?php } else { ?>
				<p>Dit is geen geldige order ID</p>

			<?php } ?>
		</div>
		<footer class="py-5 bg-dark">
			<div class="container">
				<p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
			</div>
		</footer>
	</body>
</html>