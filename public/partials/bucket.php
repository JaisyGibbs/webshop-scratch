<h2>Winkelmandje</h2>
<!-- kijken of er wat in het winkelmandje zit -->
<?php if($_SESSION['cart']['total'] != 0) {
$reset = asset('cart/reset.php')?>

<table class="table">
    <thead>
        <tr>
            <th>Product</th>
            <th>Aantal</th>
            <th>Prijs</th>
            <th></th>
        </tr>
    </thead>
    <tbody>

    <?php foreach($_SESSION['cart']['products'] as $cartProduct) { ?>
        <?php $remove = asset("cart/remove.php?id=". $cartProduct['id'])?>

        <tr>
            <td><?php echo $cartProduct['title']; ?></td>
            <td><?php echo $cartProduct['quantity']; ?></td>
            <td><?php echo $cartProduct['price']; ?></td>
            <td><button type="button" class="remove-from-cart" data-url="<?php echo $remove?>"><img src="<?php echo asset('images/trash.png')?>" width="20px"></button></td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<h3>Totaal bedrag: <?php echo number_format($_SESSION['cart']['total'], 2, '.', '')?></h3>

<button type="button" class="btn btn-primary reset-cart" data-url="<?php echo $reset?>">Empty Cart</button>
<a class="btn btn-primary betalen" href="<?php echo asset('betalen')?>">Betalen</a>
<?php } else {?>
    <p>Er staan geen artikelen in het Winkelmandje</p>
<?php }?>