<?php if (! @$_SESSION['user']){?>
    <!-- Geen user navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a href="<?php echo asset()?>">
                <img src="<?php echo asset('images/logo.png')?>" width="50pxs">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo asset('')?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo asset('registreren')?>">registreren</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo asset('login')?>">inloggen</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<?php } else {?>
    <!-- user navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a href="<?php echo asset()?>">
                <img src="<?php echo asset('images/logo.png')?>" width="50pxs">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo asset('')?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo asset('betalen')?>">Betalen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo asset('partials/logout.php')?>">Uitloggen</a>
                    </li>
                    <li>
                        <p class="nav-link active">Welkom <?php echo $_SESSION['user']['first_name'];?></p>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<?php }?>