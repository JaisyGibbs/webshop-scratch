<?php
session_start();

//classes en mollie api inladen
require 'classes/http.php';
require 'classes/databaseconnect.php';
require 'classes/cart.php';
require 'classes/account.php';
require 'vendor/autoload.php';

//mollie apikey
$mollie = new \Mollie\Api\MollieApiClient();
$mollie->setApiKey("test_DUyRVVEM5KC8wU34aFkHQHR3KvVUK9");


if(! isset($_SESSION['cart'])) {
    cart::reset();
}

http::boot();

//shortcut voor insert
function insert($query, $parameters = []){
    $db = new DB();
    return  $db->query($query, $parameters)->insert();
}

//shortcut voor select
function select($query, $parameters = []){
    $db = new DB();
    return $db->query($query, $parameters)->select();
}

//shortcut voor select
function singleselect($query, $parameters = []){
    $db = new DB();
    return $db->query($query, $parameters)->singleselect();
}

//shortcut voor updaten
function update($query, $parameters = []){
    $db = new DB();
    $db->query($query, $parameters)->update();
}

//path naar /public/
function asset($path = ""){
    return http::webroot().$path;
}

//shortcut voor css
function getcss(){
    $bootstrap = asset('vendor/bootstrap/css/bootstrap.min.css');
    $custom = asset('css/webshop.css');
    return '<link href="'. $bootstrap .'" rel="stylesheet">
            <link href="'. $custom .'" rel="stylesheet">';
}

//shortcut for login checken
function checkLogin(){
    if(account::checkLoginFromSession())
    {
        header('Location: '. asset(""));
        dd();
    }
}

function dd($text = "")
{
    if(is_array($text) || is_object($text)) {
        var_dump($text);
        die();
    }
    else {
        echo $text;
        die();
    }
}
