  <?php

Class Http
{

    public static $webroot = '';


    public static function boot()
    {
        // zoeken naar localhost
        // zoeken naar /public/
        // webroot van maken
        if($_SERVER['HTTP_HOST'] == 'localhost' && strpos($_SERVER['REQUEST_URI'], '/public/')) {
            // zoeken naar /public/
            $urlParts = explode('/public/', $_SERVER['REQUEST_URI']);
            // webroot van maken
            self::$webroot = self::httpOrHttps().$_SERVER['HTTP_HOST'].$urlParts[0].'/public/';
        }
        else {
            //REQUEST_URI erachter zetten voor ngrok
            if($_SERVER['REQUEST_URI']){
                $urlParts = explode('/public/', $_SERVER['REQUEST_URI']);
                // webroot van maken
            self::$webroot = self::httpOrHttps().$_SERVER['HTTP_HOST'].$urlParts[0].'/public/';

            }
            else{
            self::$webroot = self::httpOrHttps().$_SERVER['HTTP_HOST']."/";
            }
        }

    }

    public static function webroot()
    {
        return self::$webroot;
    }

    //check of het http of https is
    private static function httpOrHttps()
    {
        if(isset($_SERVER['HTTPS'])) {
            return 'https://';
        }
        return 'http://';
    }

}
