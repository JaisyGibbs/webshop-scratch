<?php
class Account
{
    // maakt user aan
    static function createUser($email, $password, $remember_token = 0, $first_name, $suffix_name, $last_name, $country, $city, $street, $street_number, $street_suffix, $zipcode, $created_at, $updated_at){

    	$sql = "INSERT INTO
                    users (
                        email,
                        password,
                        remember_token,
                        first_name,
                        suffix_name,
                        last_name,
                        country,
                        city,
                        street,
                        street_number,
                        street_suffix,
                        zipcode,
                        created_at,
                        updated_at
                      )
    			VALUES (
    			    :email,
      			    :password,
    			    :remember_token,
    		  	    :first_name,
    			    :suffix_name,
    			    :last_name,
    			    :country,
    			    :city,
    			    :street,
    			    :street_number,
    			    :street_suffix,
    			    :zipcode,
                    :created_at,
                    :updated_at
    			  )";

    	$parameters = [
            'email' => $email,
            'password' => $password,
            'remember_token' => $remember_token,
            'first_name' => $first_name,
            'suffix_name' => $suffix_name,
            'last_name' => $last_name,
            'country' => $country,
            'city' => $city,
            'street' => $street,
            'street_number' => $street_number,
            'street_suffix' => $street_suffix,
            'zipcode' => $zipcode,
            'created_at' => $created_at,
            'updated_at' => $updated_at
        ];

    	$id = insert($sql, $parameters);

        self::setUser($id, $first_name);

    }

    static function logout(){
        unset($_SESSION['user']);
        return "header('Location: '. asset();";
    }

    //logt de user in
    static function Login($email, $password)
    {
        $user = singleselect("SELECT * FROM users WHERE email = :email", ["email" => $email]);
        if($email === $user["email"] &&  password_verify($password, $user["password"]))
        {
            self::setUser($user['id'], $user['first_name']);
        }
        else{
            return "uw ingevoerde gegevens klopen niet";
        }
    }

    //checkt of er een user in de session zit
    static function checkLoginFromSession()
    {
        if(isset($_SESSION['user'])) {
            return true;
        }
        else{
            return false;
        }
    }

    //zet de user in de session
    static function setUser($id, $first_name)
    {
        $_SESSION['user'] = [
            'id' => $id,
            'first_name' => $first_name,
        ];
    }
}
