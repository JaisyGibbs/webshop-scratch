<?php

Class DB {

    private $conn;
    private $host = 'localhost';
    private $database = 'webshop';
    private $username = 'root';
    private $password = '';
    private $parameters = [];
    private $query;

    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$this->database, $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            dd($e->getMessage());
        }
    }

    public function getConnection()
    {
        return $this->conn;
    }

    //prapere voor query's
    public function query($query, $parameters = [])
    {
        $this->query = $this->conn->prepare($query);
        $this->parameters = $parameters;

        return $this;
    }

    //select als je meerdere records verwacht
    public function select()
    {
        $this->query->execute($this->parameters);

        $this->query->setFetchMode(PDO::FETCH_ASSOC);

        return $this->query->fetchAll();
    }

    //select voor als je 1 record verwacht
    public function singleselect()
    {
        $this->query->execute($this->parameters);

        $this->query->setFetchMode(PDO::FETCH_ASSOC);

        return $this->query->fetch();
    }


    public function insert()
    {
        try {
            $this->query->execute($this->parameters);
            return $this->conn->lastInsertId();
        }
        catch(PDOException $e) {
            dd($e->getMessage());
        }
    }


    public function update()
    {
        try {
            $this->query->execute($this->parameters);
        }
        catch(PDOException $e){
            dd($e->getMessage());
        }
    }
}




