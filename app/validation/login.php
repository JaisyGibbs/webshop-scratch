<?php

$errors = [];

$validations = [
    'required',
    'email',
    'name',
    'min',
    'max',
];


foreach($variables as $key => $checks) {

    // dd($checks);

    foreach($checks as $check) {
        // dd($check);

        $checkExploded = explode(':', $check);

        if(count($checkExploded) > 1) {


            // wel een :
            $checkFunction = 'is'.ucfirst($checkExploded[0]);
            if($error = $checkFunction($_POST[$key], $checkExploded[1], $key, $checks)) {

                if(array_key_exists($key, $errors)) {
                    array_push($errors[$key], $error);
                }
                else {
                    $errors[$key] = [$error];
                }
            }

        }
        else {
            // geen :
            $checkFunction = 'is'.ucfirst($check);

            if($error = $checkFunction($_POST[$key], $key, $checks)) {


                if(array_key_exists($key, $errors)) {
                    array_push($errors[$key], $error);
                }
                else {
                    $errors[$key] = [$error];
                }
            }
        }
    }
}


function isRequired($value, $key, $checks)
{
    if(! $value) {
        return 'U heeft geen waarde ingevuld';
    }
}

function isEmail($value, $key, $checks)
{
    if($value && ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $value)) {
        return 'U heeft geen geldig e-mail adres ingevuld';
    }
}

function isMin($value, $amount, $key, $checks)
{
    if($value && strlen($value) < $amount) {
        return 'U heeft niet voldoende tekens ingevoerd. Minimaal '.$amount;
    }
}

function isMax($value, $amount, $key, $checks)
{
    if($value && strlen($value) > $amount) {
        return 'U heeft teveel tekens ingevoerd. Maximaal '.$amount;
    }
}