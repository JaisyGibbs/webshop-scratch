<?php
try{

	//db connectie ophalen om een transactie te beginnen
	$db = new db();
	$conn = $db->getConnection();
	$conn->beginTransaction();

	//order aanmaken
	//de db class kan niet gebruikt worden anders word de query toch uitgevoerd als het fout gaat

	$order = $conn->prepare("INSERT INTO orders (amount, user_id, created_at, updated_at)
						VALUES (:amount, :user_id, :created_at, :updated_at)");
	$order->execute([
						"amount" => $_SESSION['cart']['total'],
						"user_id" => $_SESSION['user']['id'],
						"created_at" => date('Y-m-d H:i:s'),
						"updated_at" =>	date('Y-m-d H:i:s')
					]);
	$order_id = $conn->lastInsertId();

	// totaal bedrag  in een string zetten voor mollie
	$totaal = strval($_SESSION['cart']['total']);

	// mollie

	$payment = $mollie->payments->create([
	    "amount" => [
	        "currency" => "EUR",
	        "value" => $totaal
	    ],
	    "description" => "My first API payment",
	    "redirectUrl" => asset("order/".$order_id),
	    "webhookUrl"  => asset("webhook.php"),
	    "metadata" => [
            "order_id" => $order_id
        ]
	]);

	// order updaten

	$update = $conn->prepare("UPDATE orders SET   mollie_id = :mollie_id,
								payment_status = :payment_status,
								updated_at = :updated_at
								WHERE id = :id");
	$update->execute(
		[
			"mollie_id" => $payment->id,
			"payment_status" => $payment->status,
			"updated_at" => date('Y-m-d H:i:s'),
			"id" => $order_id
		]);

	// door elk product heen loopen om het te inserten

	foreach($_SESSION['cart']['products'] as $product){
		$orders_products = $conn->prepare("INSERT INTO orders_products (product_id, order_id, price, quantity, created_at, updated_at)
					VALUES (:product_id, :order_id, :price, :quantity, :created_at, :updated_at)");

		$orders_products->execute(
			[
			 	"product_id" => $product['id'],
			 	"order_id" => $order_id,
			 	"price" => $product['price'],
			 	"quantity" => $product['quantity'],
			 	"created_at" => date('Y-m-d H:i:s'),
				"updated_at" =>	date('Y-m-d H:i:s')
			]);
	}

	// commit, cart reseten en doorsturen naar mollie
	$conn->commit();
	cart::reset();
	header("Location: " . $payment->getCheckoutUrl(), true, 303);
}
catch(Exception $e){
	// als er wat fout gaat word er niks uitgevoerd

	dd($e->getMessage());
	$conn->rollback();
}
?>
